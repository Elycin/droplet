﻿Imports System.Net
Imports System.Runtime.CompilerServices
Imports System.Web.Script.Serialization
Imports System.Threading

Public Class rMain
    Dim urlformat As String = ("client_id=" + My.Settings.clientid.ToString() + "&api_key=" + My.Settings.apikey.ToString())
    Dim client As New Net.WebClient
    Dim si As Integer


    Public Sub ListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles dropletList.SelectedIndexChanged


        Dim response As String = client.DownloadString(String.Concat("https://api.digitalocean.com/v1/droplets/?", Me.urlformat.ToString()))
        Dim jss As JavaScriptSerializer = New JavaScriptSerializer()
        Dim result As Object = RuntimeHelpers.GetObjectValue((New JavaScriptSerializer()).Deserialize(Of Object)(response))
        si = Me.dropletList.SelectedIndex
        If (Me.dropletList.Items.Count > 0) Then
            Try

                dropletid.Text = result("droplets")(si)("id")
                dropletname.Text = result("droplets")(si)("name")
                dropletosid.Text = result("droplets")(si)("image_id")


                Try
                    Dim IDresponse As String = client.DownloadString("https://api.digitalocean.com/v1/images/" + dropletosid.Text + "/?" + urlformat)
                    Dim IDresult As Object = RuntimeHelpers.GetObjectValue((New JavaScriptSerializer()).Deserialize(Of Object)(IDresponse))

                    dropletsnapshotname.Text = IDresult("image")("name")
                    dropletdistribution.Text = IDresult("image")("distribution")
                    dropletsize.Text = result("droplets")(si)("size_id")
                Catch ex2 As Exception
                    MsgBox("Droplet " + si + "Has an Abnormal status. if you created this from an image and deleted it, this may be why")
                End Try


                If dropletsize.Text = "66" Then
                    dropletsize.Text = "512MB"
                ElseIf dropletsize.Text = "63" Then
                    dropletsize.Text = "1GB"
                ElseIf dropletsize.Text = "62" Then
                    dropletsize.Text = "2GB"
                ElseIf dropletsize.Text = "64" Then
                    dropletsize.Text = "4GB"
                ElseIf dropletsize.Text = "65" Then
                    dropletsize.Text = "8GB"
                ElseIf dropletsize.Text = "61" Then
                    dropletsize.Text = "16GB"
                ElseIf dropletsize.Text = "60" Then
                    dropletsize.Text = "32GB"
                ElseIf dropletsize.Text = "70" Then
                    dropletsize.Text = "48GB"
                ElseIf dropletsize.Text = "69" Then
                    dropletsize.Text = "64GB"
                ElseIf dropletsize.Text = "78" Then
                    dropletsize.Text = "96GB"
                End If

                dropletregion.Text = result("droplets")(si)("region_id")
                Dim REGIONresponse As String = client.DownloadString("https://api.digitalocean.com/v1/regions/?" + urlformat)
                Dim REGIONresult As Object = RuntimeHelpers.GetObjectValue((New JavaScriptSerializer()).Deserialize(Of Object)(REGIONresponse))
                Dim i2 As Integer = 0
redo2:

                If i2 = dropletregion.Text Then
                    dropletregion.Text = REGIONresult("regions")(i2 - 2)("name")
                Else
                    i2 = Val(i2) + 1
                    GoTo redo2
                End If



                dropletsbackupboolean.Text = result("droplets")(si)("backups_active")
                dropletip.Text = result("droplets")(si)("ip_address")
                dropletprivateip.Text = result("droplets")(si)("private_ip_address")
                If dropletprivateip.Text = "" Then
                    dropletprivateip.Text = "Private Networking is not Enabled."
                End If
                dropletlocked.Text = result("droplets")(si)("locked")

                If dropletlocked.Text = "True" Then
                    dropletlocked.ForeColor = Color.Red
                    lockedstatus.Text = "This droplet is currently locked, you currently cannot interact with this droplet."
                    action_boot.Enabled = False
                    action_reboot_force.Enabled = False
                    action_reboot_normal.Enabled = False
                    action_shutdown.Enabled = False
                    action_delete.Enabled = False
                    action_create_snapshot.Enabled = False
                ElseIf dropletlocked.Text = "False" Then
                    dropletlocked.ForeColor = Color.LimeGreen
                    lockedstatus.Text = "" 'make invisible.
                    action_boot.Enabled = True
                    action_reboot_force.Enabled = True
                    action_reboot_normal.Enabled = True
                    action_shutdown.Enabled = True
                    action_delete.Enabled = True
                    action_create_snapshot.Enabled = True
                Else
                    dropletlocked.Text = "Unknown Status: " + dropletlocked.Text
                    dropletlocked.ForeColor = Color.Purple
                End If
                dropletstatus.Text = result("droplets")(si)("status")
                If dropletstatus.Text = "active" Then
                    dropletstatus.Text = "Active"
                    If dropletlocked.Text = "True" Then
                        action_boot.Enabled = False
                        action_shutdown.Enabled = False
                    Else
                        action_boot.Enabled = True
                        action_shutdown.Enabled = True
                    End If
                    dropletstatus.ForeColor = Color.LimeGreen
                    action_boot.Enabled = False
                    action_shutdown.Enabled = True
                ElseIf dropletstatus.Text = "off" Then
                    dropletstatus.Text = "Shutdown"
                    If dropletlocked.Text = "True" Then
                        action_boot.Enabled = False
                        action_shutdown.Enabled = False
                    Else
                        action_boot.Enabled = True
                        action_shutdown.Enabled = True
                    End If
                    dropletstatus.ForeColor = Color.Red
                    action_shutdown.Enabled = False

                    




                Else
                    dropletstatus.Text = "Unknown Status: " + dropletstatus.Text
                    dropletstatus.ForeColor = Color.Purple
                    action_boot.Enabled = True
                End If





                dropletcreationdate.Text = result("droplets")(si)("created_at")
            Catch ex As Exception

            End Try


        End If
    End Sub

    Sub rMain_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Static Dim clientid As String = My.Settings.clientid.ToString
        Static Dim apikey As String = My.Settings.apikey.ToString
        Try


            Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/?" + urlformat)
            Dim jss As JavaScriptSerializer = New JavaScriptSerializer()
            Dim result As Object = RuntimeHelpers.GetObjectValue((New JavaScriptSerializer()).Deserialize(Of Object)(response))
            Dim i As Integer = 0
            Try
redo1:
                While Not i = 2147483647 'a bit outrageous for a high integer, but oh well. lol
                    dropletList.Items.Add(result("droplets")(i)("name"))
                    i = Val(i) + 1
                End While

            Catch ex As Exception
                '
            End Try
            dropletList.SelectedIndex = 0
            'MsgBox(dropletList.Items.Count.ToString + " Droplets Found") 'debug
        Catch ex As Exception
            'bounces back the critical update.
        End Try
    End Sub


    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles action_boot.Click
        'boot
        Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/power_on/?" + urlformat)
        MsgBox("Droplet Booted successfully.")
        Thread.Sleep(5000)
        dropletList.SelectedIndex = 0

    End Sub

    Private Sub action_shutdown_Click(sender As Object, e As EventArgs) Handles action_shutdown.Click
        'power off
        Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/power_off/?" + urlformat)
        MsgBox("Droplet has been shut down successfully.")
        Thread.Sleep(5000)
        dropletList.SelectedIndex = 0
    End Sub

    Private Sub action_reboot_Click(sender As Object, e As EventArgs) Handles action_reboot_force.Click
        Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/reboot/?" + urlformat)
        MsgBox("The droplet will now Forcefully restart. changes may be lost.")
        Thread.Sleep(20000)
        dropletList.SelectedIndex = 0
        MsgBox("Droplet has rebooted successfully.")
    End Sub

    Private Sub action_reboot_normal_Click(sender As Object, e As EventArgs) Handles action_reboot_normal.Click
        Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/power_cycle/?" + urlformat)
        MsgBox("The droplet will now restart.")
        Thread.Sleep(20000)
        dropletList.SelectedIndex = 0
        MsgBox("Droplet has rebooted successfully.")
    End Sub

    Private Sub action_delete_Click(sender As Object, e As EventArgs) Handles action_delete.Click
        Dim IBresult = MsgBox("Are you sure you want to delete this droplet?" + vbNewLine + "It will NOT be recoverable beyond this point if you select 'Yes'", MsgBoxStyle.YesNo)
        If IBresult = DialogResult.Yes Then
            Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/destroy/?" + urlformat)
        Else
            MsgBox("Droplet has not been destroyed.")
        End If
    End Sub

    Private Sub action_create_snapshot_Click(sender As Object, e As EventArgs) Handles action_create_snapshot.Click
        Dim IBresult = InputBox("Please name the snapshot:")
        If IBresult = "" Then
            MsgBox("Snapshot not created")
        Else

            MsgBox("Notice: this will take a few minutes depending on the size of your droplet. " + vbNewLine + vbNewLine + "THE SOFTWARE MAY FREEZE, DO NOT BE ALARMED, YOUR DROPLET WILL POWER BACK ON AFTER SNAPSHOT HAS BEEN CREATED.")
            Dim poweroff = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/power_off/?" + urlformat)
            Thread.Sleep(15000)
            Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/" + dropletid.Text + "/snapshot/?name=" + IBresult.ToString + "&" + urlformat)
            Thread.Sleep(5000)
            dropletList.SelectedItem = si
            While dropletlocked.Text = "True"
                dropletList.SelectedItem = si
            End While
            MsgBox("Snapshot " + IBresult.ToString + " has been created successfully, the time taken to create the snapshot depends on the size of your droplet. it may take anywhere from 2 to 20 minutes to do this process. when it is done, your droplet will power back on.")
        End If
    End Sub

    Private Sub HelpToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles HelpToolStripMenuItem.Click

    End Sub

    Private Sub CheckForUpdatesToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CheckForUpdatesToolStripMenuItem.Click
        Try


            Dim latestVerstion As String = client.DownloadString("http://nodesoftware.com/dropletplus/version.txt")
            Dim latestVerstionType As String = client.DownloadString("http://nodesoftware.com/dropletplus/type.txt")
            Dim latestVerstionMessage As String = client.DownloadString("http://nodesoftware.com/dropletplus/message.txt")
            If latestVerstion.ToString > My.Application.Info.Version.ToString Then
                MsgBox("Node Software has released verison " + latestVerstion.ToString + " of Droplet+ as an " + latestVerstionType.ToString + " update." + vbNewLine + vbNewLine + "Update Message: " + vbNewLine + vbNewLine + latestVerstionMessage.ToString)

                If latestVerstionType.Contains("critical") Then
                    MsgBox("As this is a critical update, You can no longer use this version.")
                    Me.Close()
                End If
            Else
                MsgBox("No update available.")
            End If
        Catch ex As Exception
            MsgBox("Update server couldn't be contacted. assuming latest version.")
        End Try
    End Sub

    Private Sub VisitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles VisitToolStripMenuItem.Click
        Process.Start("http://digitalocean.com/")
    End Sub

    Private Sub DropletToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DropletToolStripMenuItem.Click

    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click
        Application.Exit()

    End Sub

    Private Sub LogoutToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LogoutToolStripMenuItem.Click
        My.Settings.clientid = ""
        My.Settings.apikey = ""
        My.Settings.Save()
        login.Show()
        Me.Close()

    End Sub

    Private Sub WikiToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles WikiToolStripMenuItem.Click
        MsgBox("Feature coming soon")
    End Sub

    Private Sub dropletstatus_Click(sender As Object, e As EventArgs) Handles dropletstatus.Click
       
    End Sub

    Private Sub RefreshToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RefreshToolStripMenuItem.Click

    End Sub

    Private Sub ListOfDropletsToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListOfDropletsToolStripMenuItem.Click
        dropletList.Items.Clear()
        
        Static Dim clientid As String = My.Settings.clientid.ToString
        Static Dim apikey As String = My.Settings.apikey.ToString
        Try


            Dim response As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/?" + urlformat)
            Dim jss As JavaScriptSerializer = New JavaScriptSerializer()
            Dim result As Object = RuntimeHelpers.GetObjectValue((New JavaScriptSerializer()).Deserialize(Of Object)(response))
            Dim i As Integer = 0
            Try
redo1:
                While Not i = 2147483647 'a bit outrageous for a high integer, but oh well. lol
                    dropletList.Items.Add(result("droplets")(i)("name"))
                    i = Val(i) + 1
                End While

            Catch ex As Exception
                '
            End Try
            dropletList.SelectedIndex = 0
        Catch ex As Exception

        End Try

    End Sub

  
End Class