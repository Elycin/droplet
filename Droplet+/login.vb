﻿


'Droplet+ is created by Andrew Haughie of Node Software
'http://nodesoftware.com/
'This software is licensed under GPL v2 http://www.gnu.org/licenses/gpl-2.0.html








Imports System.Runtime.CompilerServices
Imports System.Web.Script.Serialization

Public Class login

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        clientid.Text = My.Settings.clientid
        ApiKey.Text = My.Settings.apikey
        'check for updates
        Dim client As New Net.WebClient
        Try

       
        Dim latestVerstion As String = client.DownloadString("http://nodesoftware.com/dropletplus/version.txt")
        Dim latestVerstionType As String = client.DownloadString("http://nodesoftware.com/dropletplus/type.txt")
        Dim latestVerstionMessage As String = client.DownloadString("http://nodesoftware.com/dropletplus/message.txt")
        If latestVerstion.ToString > My.Application.Info.Version.ToString Then
            MsgBox("Node Software has released verison " + latestVerstion.ToString + " of Droplet+ as an " + latestVerstionType.ToString + " update." + vbNewLine + vbNewLine + "Update Message: " + vbNewLine + vbNewLine + latestVerstionMessage.ToString)

            If latestVerstionType.Contains("critical") Then
                MsgBox("As this is a critical update, You can no longer use this version.")
                Me.Close()
            End If
            End If
        Catch ex As Exception
            MsgBox("Update server couldn't be contacted. assuming latest version.")
        End Try
    End Sub

    Private Sub clientid_TextChanged(sender As Object, e As EventArgs) Handles clientid.TextChanged

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim urlformat As String = ("client_id=" + clientid.Text + "&api_key=" + ApiKey.Text)
        Dim client As New Net.WebClient
        My.Settings.clientid = clientid.Text
        My.Settings.apikey = ApiKey.Text
        My.Settings.Save()
        Try
            Dim Status As String = client.DownloadString("https://api.digitalocean.com/v1/droplets/?" + urlformat)
            If Status.Contains("OK") Then
                rMain.Show()
                Me.Close()
            End If
        Catch ex As Exception
            MsgBox("401 Error - Unauthorized, Please use a valid Client ID and API Key")
        End Try



    End Sub

    Private Sub Label3_Click(sender As Object, e As EventArgs) Handles Label3.Click
        Process.Start("https://bitbucket.org/Elycin/droplet")
    End Sub
End Class
