﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class rMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.dropletList = New System.Windows.Forms.ListBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.dropletid = New System.Windows.Forms.Label()
        Me.dropletname = New System.Windows.Forms.Label()
        Me.dropletosid = New System.Windows.Forms.Label()
        Me.dropletsnapshotname = New System.Windows.Forms.Label()
        Me.dropletdistribution = New System.Windows.Forms.Label()
        Me.dropletsize = New System.Windows.Forms.Label()
        Me.dropletregion = New System.Windows.Forms.Label()
        Me.dropletsbackupboolean = New System.Windows.Forms.Label()
        Me.dropletip = New System.Windows.Forms.Label()
        Me.dropletprivateip = New System.Windows.Forms.Label()
        Me.dropletlocked = New System.Windows.Forms.Label()
        Me.dropletstatus = New System.Windows.Forms.Label()
        Me.dropletcreationdate = New System.Windows.Forms.Label()
        Me.action_boot = New System.Windows.Forms.Button()
        Me.action_shutdown = New System.Windows.Forms.Button()
        Me.action_reboot_force = New System.Windows.Forms.Button()
        Me.action_delete = New System.Windows.Forms.Button()
        Me.action_create_snapshot = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.action_reboot_normal = New System.Windows.Forms.Button()
        Me.lockedstatus = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.DropletToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LogoutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.WikiToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DigitalOceanToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.VisitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.HelpToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CheckForUpdatesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.RefreshToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListOfDropletsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dropletList
        '
        Me.dropletList.FormattingEnabled = True
        Me.dropletList.Location = New System.Drawing.Point(12, 116)
        Me.dropletList.Name = "dropletList"
        Me.dropletList.Size = New System.Drawing.Size(120, 485)
        Me.dropletList.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 102)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Droplets:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(143, 102)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(99, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Droplet Information:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(143, 126)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 13)
        Me.Label3.TabIndex = 4
        Me.Label3.Text = "Droplet Numerical ID:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(143, 139)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(75, 13)
        Me.Label4.TabIndex = 5
        Me.Label4.Text = "Droplet Name:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(143, 152)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(148, 13)
        Me.Label5.TabIndex = 6
        Me.Label5.Text = "Operating System Information:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(200, 165)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(107, 13)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = "Operating System ID:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(200, 178)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(135, 13)
        Me.Label7.TabIndex = 8
        Me.Label7.Text = "Snapshot/Template Name:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(200, 191)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(90, 13)
        Me.Label8.TabIndex = 9
        Me.Label8.Text = "Linux Distribution:"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(143, 215)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(30, 13)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Size:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(143, 228)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(44, 13)
        Me.Label10.TabIndex = 11
        Me.Label10.Text = "Region:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(143, 241)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(94, 13)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "Backups Enabled:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(143, 254)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(93, 13)
        Me.Label12.TabIndex = 13
        Me.Label12.Text = "Public IP Address:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(143, 267)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(97, 13)
        Me.Label13.TabIndex = 14
        Me.Label13.Text = "Private IP Address:"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(143, 280)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(83, 13)
        Me.Label14.TabIndex = 15
        Me.Label14.Text = "Locked Droplet:"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(143, 293)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(40, 13)
        Me.Label15.TabIndex = 16
        Me.Label15.Text = "Status:"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(143, 306)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(75, 13)
        Me.Label16.TabIndex = 17
        Me.Label16.Text = "Creation Date:"
        '
        'dropletid
        '
        Me.dropletid.AutoSize = True
        Me.dropletid.Location = New System.Drawing.Point(380, 126)
        Me.dropletid.Name = "dropletid"
        Me.dropletid.Size = New System.Drawing.Size(23, 13)
        Me.dropletid.TabIndex = 18
        Me.dropletid.Text = "null"
        '
        'dropletname
        '
        Me.dropletname.AutoSize = True
        Me.dropletname.Location = New System.Drawing.Point(380, 139)
        Me.dropletname.Name = "dropletname"
        Me.dropletname.Size = New System.Drawing.Size(23, 13)
        Me.dropletname.TabIndex = 19
        Me.dropletname.Text = "null"
        '
        'dropletosid
        '
        Me.dropletosid.AutoSize = True
        Me.dropletosid.Location = New System.Drawing.Point(380, 165)
        Me.dropletosid.Name = "dropletosid"
        Me.dropletosid.Size = New System.Drawing.Size(23, 13)
        Me.dropletosid.TabIndex = 20
        Me.dropletosid.Text = "null"
        '
        'dropletsnapshotname
        '
        Me.dropletsnapshotname.AutoSize = True
        Me.dropletsnapshotname.Location = New System.Drawing.Point(380, 178)
        Me.dropletsnapshotname.Name = "dropletsnapshotname"
        Me.dropletsnapshotname.Size = New System.Drawing.Size(23, 13)
        Me.dropletsnapshotname.TabIndex = 21
        Me.dropletsnapshotname.Text = "null"
        '
        'dropletdistribution
        '
        Me.dropletdistribution.AutoSize = True
        Me.dropletdistribution.Location = New System.Drawing.Point(380, 191)
        Me.dropletdistribution.Name = "dropletdistribution"
        Me.dropletdistribution.Size = New System.Drawing.Size(23, 13)
        Me.dropletdistribution.TabIndex = 22
        Me.dropletdistribution.Text = "null"
        '
        'dropletsize
        '
        Me.dropletsize.AutoSize = True
        Me.dropletsize.Location = New System.Drawing.Point(380, 215)
        Me.dropletsize.Name = "dropletsize"
        Me.dropletsize.Size = New System.Drawing.Size(23, 13)
        Me.dropletsize.TabIndex = 23
        Me.dropletsize.Text = "null"
        '
        'dropletregion
        '
        Me.dropletregion.AutoSize = True
        Me.dropletregion.Location = New System.Drawing.Point(380, 228)
        Me.dropletregion.Name = "dropletregion"
        Me.dropletregion.Size = New System.Drawing.Size(23, 13)
        Me.dropletregion.TabIndex = 24
        Me.dropletregion.Text = "null"
        '
        'dropletsbackupboolean
        '
        Me.dropletsbackupboolean.AutoSize = True
        Me.dropletsbackupboolean.Location = New System.Drawing.Point(380, 241)
        Me.dropletsbackupboolean.Name = "dropletsbackupboolean"
        Me.dropletsbackupboolean.Size = New System.Drawing.Size(23, 13)
        Me.dropletsbackupboolean.TabIndex = 25
        Me.dropletsbackupboolean.Text = "null"
        '
        'dropletip
        '
        Me.dropletip.AutoSize = True
        Me.dropletip.Location = New System.Drawing.Point(380, 254)
        Me.dropletip.Name = "dropletip"
        Me.dropletip.Size = New System.Drawing.Size(23, 13)
        Me.dropletip.TabIndex = 26
        Me.dropletip.Text = "null"
        '
        'dropletprivateip
        '
        Me.dropletprivateip.AutoSize = True
        Me.dropletprivateip.Location = New System.Drawing.Point(380, 267)
        Me.dropletprivateip.Name = "dropletprivateip"
        Me.dropletprivateip.Size = New System.Drawing.Size(23, 13)
        Me.dropletprivateip.TabIndex = 27
        Me.dropletprivateip.Text = "null"
        '
        'dropletlocked
        '
        Me.dropletlocked.AutoSize = True
        Me.dropletlocked.Location = New System.Drawing.Point(380, 280)
        Me.dropletlocked.Name = "dropletlocked"
        Me.dropletlocked.Size = New System.Drawing.Size(23, 13)
        Me.dropletlocked.TabIndex = 28
        Me.dropletlocked.Text = "null"
        '
        'dropletstatus
        '
        Me.dropletstatus.AutoSize = True
        Me.dropletstatus.Location = New System.Drawing.Point(380, 293)
        Me.dropletstatus.Name = "dropletstatus"
        Me.dropletstatus.Size = New System.Drawing.Size(23, 13)
        Me.dropletstatus.TabIndex = 29
        Me.dropletstatus.Text = "null"
        '
        'dropletcreationdate
        '
        Me.dropletcreationdate.AutoSize = True
        Me.dropletcreationdate.Location = New System.Drawing.Point(380, 306)
        Me.dropletcreationdate.Name = "dropletcreationdate"
        Me.dropletcreationdate.Size = New System.Drawing.Size(23, 13)
        Me.dropletcreationdate.TabIndex = 30
        Me.dropletcreationdate.Text = "null"
        '
        'action_boot
        '
        Me.action_boot.Location = New System.Drawing.Point(146, 462)
        Me.action_boot.Name = "action_boot"
        Me.action_boot.Size = New System.Drawing.Size(161, 23)
        Me.action_boot.TabIndex = 31
        Me.action_boot.Text = "Boot Droplet"
        Me.action_boot.UseVisualStyleBackColor = True
        '
        'action_shutdown
        '
        Me.action_shutdown.Location = New System.Drawing.Point(146, 491)
        Me.action_shutdown.Name = "action_shutdown"
        Me.action_shutdown.Size = New System.Drawing.Size(161, 23)
        Me.action_shutdown.TabIndex = 32
        Me.action_shutdown.Text = "Shutdown Droplet"
        Me.action_shutdown.UseVisualStyleBackColor = True
        '
        'action_reboot_force
        '
        Me.action_reboot_force.Location = New System.Drawing.Point(146, 549)
        Me.action_reboot_force.Name = "action_reboot_force"
        Me.action_reboot_force.Size = New System.Drawing.Size(161, 23)
        Me.action_reboot_force.TabIndex = 33
        Me.action_reboot_force.Text = "Force Restart Droplet"
        Me.action_reboot_force.UseVisualStyleBackColor = True
        '
        'action_delete
        '
        Me.action_delete.Location = New System.Drawing.Point(373, 462)
        Me.action_delete.Name = "action_delete"
        Me.action_delete.Size = New System.Drawing.Size(161, 23)
        Me.action_delete.TabIndex = 34
        Me.action_delete.Text = "Delete Droplet"
        Me.action_delete.UseVisualStyleBackColor = True
        '
        'action_create_snapshot
        '
        Me.action_create_snapshot.Location = New System.Drawing.Point(373, 491)
        Me.action_create_snapshot.Name = "action_create_snapshot"
        Me.action_create_snapshot.Size = New System.Drawing.Size(161, 23)
        Me.action_create_snapshot.TabIndex = 35
        Me.action_create_snapshot.Text = "Create Snapshot"
        Me.action_create_snapshot.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.Droplet_.My.Resources.Resources.logo_digitalocean_999daba3f7ccfa71c3f836aa98d0b1dc_3_
        Me.PictureBox1.Location = New System.Drawing.Point(12, 27)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(368, 72)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 0
        Me.PictureBox1.TabStop = False
        '
        'action_reboot_normal
        '
        Me.action_reboot_normal.Location = New System.Drawing.Point(146, 520)
        Me.action_reboot_normal.Name = "action_reboot_normal"
        Me.action_reboot_normal.Size = New System.Drawing.Size(161, 23)
        Me.action_reboot_normal.TabIndex = 36
        Me.action_reboot_normal.Text = "Restart Droplet"
        Me.action_reboot_normal.UseVisualStyleBackColor = True
        '
        'lockedstatus
        '
        Me.lockedstatus.AutoSize = True
        Me.lockedstatus.Location = New System.Drawing.Point(138, 375)
        Me.lockedstatus.Name = "lockedstatus"
        Me.lockedstatus.Size = New System.Drawing.Size(289, 13)
        Me.lockedstatus.TabIndex = 37
        Me.lockedstatus.Text = "This will be modified to tell the user that the droplet is locked"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DropletToolStripMenuItem, Me.DigitalOceanToolStripMenuItem, Me.RefreshToolStripMenuItem, Me.HelpToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(771, 24)
        Me.MenuStrip1.TabIndex = 38
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'DropletToolStripMenuItem
        '
        Me.DropletToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem, Me.LogoutToolStripMenuItem, Me.WikiToolStripMenuItem})
        Me.DropletToolStripMenuItem.Name = "DropletToolStripMenuItem"
        Me.DropletToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.DropletToolStripMenuItem.Text = "Droplet+"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'LogoutToolStripMenuItem
        '
        Me.LogoutToolStripMenuItem.Name = "LogoutToolStripMenuItem"
        Me.LogoutToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.LogoutToolStripMenuItem.Text = "Logout"
        '
        'WikiToolStripMenuItem
        '
        Me.WikiToolStripMenuItem.Name = "WikiToolStripMenuItem"
        Me.WikiToolStripMenuItem.Size = New System.Drawing.Size(112, 22)
        Me.WikiToolStripMenuItem.Text = "Wiki"
        '
        'DigitalOceanToolStripMenuItem
        '
        Me.DigitalOceanToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.VisitToolStripMenuItem})
        Me.DigitalOceanToolStripMenuItem.Name = "DigitalOceanToolStripMenuItem"
        Me.DigitalOceanToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.DigitalOceanToolStripMenuItem.Text = "DigitalOcean"
        '
        'VisitToolStripMenuItem
        '
        Me.VisitToolStripMenuItem.Name = "VisitToolStripMenuItem"
        Me.VisitToolStripMenuItem.Size = New System.Drawing.Size(96, 22)
        Me.VisitToolStripMenuItem.Text = "Visit"
        '
        'HelpToolStripMenuItem
        '
        Me.HelpToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CheckForUpdatesToolStripMenuItem})
        Me.HelpToolStripMenuItem.Name = "HelpToolStripMenuItem"
        Me.HelpToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.HelpToolStripMenuItem.Text = "Help"
        '
        'CheckForUpdatesToolStripMenuItem
        '
        Me.CheckForUpdatesToolStripMenuItem.Name = "CheckForUpdatesToolStripMenuItem"
        Me.CheckForUpdatesToolStripMenuItem.Size = New System.Drawing.Size(170, 22)
        Me.CheckForUpdatesToolStripMenuItem.Text = "Check for updates"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(135, 446)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(79, 13)
        Me.Label17.TabIndex = 39
        Me.Label17.Text = "Power Options:"
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(370, 446)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(72, 13)
        Me.Label18.TabIndex = 40
        Me.Label18.Text = "Management:"
        '
        'RefreshToolStripMenuItem
        '
        Me.RefreshToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListOfDropletsToolStripMenuItem})
        Me.RefreshToolStripMenuItem.Name = "RefreshToolStripMenuItem"
        Me.RefreshToolStripMenuItem.Size = New System.Drawing.Size(58, 20)
        Me.RefreshToolStripMenuItem.Text = "Refresh"
        '
        'ListOfDropletsToolStripMenuItem
        '
        Me.ListOfDropletsToolStripMenuItem.Name = "ListOfDropletsToolStripMenuItem"
        Me.ListOfDropletsToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.ListOfDropletsToolStripMenuItem.Text = "List of Droplets"
        '
        'rMain
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(771, 605)
        Me.Controls.Add(Me.Label18)
        Me.Controls.Add(Me.Label17)
        Me.Controls.Add(Me.lockedstatus)
        Me.Controls.Add(Me.action_reboot_normal)
        Me.Controls.Add(Me.action_create_snapshot)
        Me.Controls.Add(Me.action_delete)
        Me.Controls.Add(Me.action_reboot_force)
        Me.Controls.Add(Me.action_shutdown)
        Me.Controls.Add(Me.action_boot)
        Me.Controls.Add(Me.dropletcreationdate)
        Me.Controls.Add(Me.dropletstatus)
        Me.Controls.Add(Me.dropletlocked)
        Me.Controls.Add(Me.dropletprivateip)
        Me.Controls.Add(Me.dropletip)
        Me.Controls.Add(Me.dropletsbackupboolean)
        Me.Controls.Add(Me.dropletregion)
        Me.Controls.Add(Me.dropletsize)
        Me.Controls.Add(Me.dropletdistribution)
        Me.Controls.Add(Me.dropletsnapshotname)
        Me.Controls.Add(Me.dropletosid)
        Me.Controls.Add(Me.dropletname)
        Me.Controls.Add(Me.dropletid)
        Me.Controls.Add(Me.Label16)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Label12)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.dropletList)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "rMain"
        Me.Text = "Droplet+ Control Panel for DigitalOcean"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents dropletList As System.Windows.Forms.ListBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label14 As System.Windows.Forms.Label
    Friend WithEvents Label15 As System.Windows.Forms.Label
    Friend WithEvents Label16 As System.Windows.Forms.Label
    Friend WithEvents dropletid As System.Windows.Forms.Label
    Friend WithEvents dropletname As System.Windows.Forms.Label
    Friend WithEvents dropletosid As System.Windows.Forms.Label
    Friend WithEvents dropletsnapshotname As System.Windows.Forms.Label
    Friend WithEvents dropletdistribution As System.Windows.Forms.Label
    Friend WithEvents dropletsize As System.Windows.Forms.Label
    Friend WithEvents dropletregion As System.Windows.Forms.Label
    Friend WithEvents dropletsbackupboolean As System.Windows.Forms.Label
    Friend WithEvents dropletip As System.Windows.Forms.Label
    Friend WithEvents dropletprivateip As System.Windows.Forms.Label
    Friend WithEvents dropletlocked As System.Windows.Forms.Label
    Friend WithEvents dropletstatus As System.Windows.Forms.Label
    Friend WithEvents dropletcreationdate As System.Windows.Forms.Label
    Friend WithEvents action_boot As System.Windows.Forms.Button
    Friend WithEvents action_shutdown As System.Windows.Forms.Button
    Friend WithEvents action_reboot_force As System.Windows.Forms.Button
    Friend WithEvents action_delete As System.Windows.Forms.Button
    Friend WithEvents action_create_snapshot As System.Windows.Forms.Button
    Friend WithEvents action_reboot_normal As System.Windows.Forms.Button
    Friend WithEvents lockedstatus As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents DropletToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents DigitalOceanToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents HelpToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CheckForUpdatesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents VisitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents LogoutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents WikiToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label17 As System.Windows.Forms.Label
    Friend WithEvents Label18 As System.Windows.Forms.Label
    Friend WithEvents RefreshToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListOfDropletsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
